// bài 1
const diemChuan = () => {
  var diemchuan = document.getElementById("diemchuan").value * 1;
  var khuvuc = document.getElementById("khuvuc").value * 1;
  var doituong = document.getElementById("doituong").value * 1;
  var monthu1 = document.getElementById("monthu1").value * 1;
  var monthu2 = document.getElementById("monthu2").value * 1;
  var monthu3 = document.getElementById("monthu3").value * 1;

  if (diemchuan > 0 && diemchuan <= 30) {
    var tongdiem = khuvuc + doituong + monthu1 + monthu2 + monthu3;
    if (
      monthu1 > 0 &&
      monthu2 > 0 &&
      monthu3 > 0 &&
      monthu1 <= 10 &&
      monthu2 <= 10 &&
      monthu3 <= 10
    ) {
      if (tongdiem >= diemchuan) {
        document.getElementById(
          "tinhdiemchuan"
        ).innerHTML = `Bạn đã đậu. Tổng điểm: ${tongdiem}`;
      } else {
        document.getElementById(
          "tinhdiemchuan"
        ).innerHTML = `Bạn đã rớt. Tổng điểm: ${tongdiem}`;
        console.log(tongdiem);
      }
    } else {
      document.getElementById(
        "tinhdiemchuan"
      ).innerHTML = `Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0`;
    }
  } else {
    alert("Vui lòng nhập điểm chuẩn >= 30 và < 0");
  }
};

// bài 2
const tinhTienDien = () => {
  var sokw = document.getElementById("sokw").value * 1;
  var tongtien = "";
  if (sokw <= 0) {
    alert("Vui lòng nhập số điện sử dụng");
  } else {
    if (sokw <= 50) {
      tongtien = sokw * 500;
    } else if (sokw <= 100) {
      tongtien = 50 * 500 + (sokw - 50) * 650;
    } else if (sokw <= 200) {
      tongtien = 50 * 500 + 50 * 650 + (sokw - 100) * 850;
    } else if (sokw <= 350) {
      tongtien = 50 * 500 + 50 * 650 + 100 * 850 + (sokw - 200) * 1100;
    } else {
      tongtien =
        50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (sokw - 350) * 1300;
    }
  }
  document.getElementById(
    "tinhtiendien"
  ).innerHTML = `Tổng tiền điện : ${tongtien} d/kw`;
};
